# m-json-stream
> Transformation stream that takes [Line Delimited JSON](https://en.wikipedia.org/wiki/Line_Delimited_JSON) and returns JSON objects.

## Requirements

* ```git```
* ```npm``` and ```node.js```

## Install

```sh
$ npm install bitbucket:mieszko4/node-m-json-stream
```


## Usage

```js
var JsonStream = require('m-json-stream');
var stream = require('stream');

var source = new stream.Readable();
var destination = new JsonStream({strict: false});

source.pipe(destination);

destination.on('data', function (object) {
  console.log(object);
});

//push string Json containing two objects
source.push('{"a",:'); //contains mistake: unexpected token ,
source.push('23}');
source.push('\n{"b": 88');
source.push('}');
source.push(null);
//since strict is false, outputs second object only:
//{ b: 88 }
```

## Development


```sh
$ git clone https://mieszko4@bitbucket.org/mieszko4/node-m-json-stream.git
$ cd node-m-json-stream
$ npm install
$ npm test
```


## License

Apache-2.0 © [Mieszko4]()