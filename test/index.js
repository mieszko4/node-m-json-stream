import assert from 'assert';
import JsonStream from '../lib';
import { Readable } from 'stream';

let assertPipe = function (inputChunks, expectedObjects, options, done) {
  let source = new Readable();
  let actualObjects = [];

  let destination = new JsonStream(options);
  destination.on('data', function (object) {
    actualObjects.push(object);
  });
  destination.on('end', function () {
    assert.deepEqual(expectedObjects, actualObjects);
    done();
  });
  destination.on('error', function () {
    assert.deepEqual(expectedObjects, actualObjects);
    done();
  });

  source.pipe(destination);
  inputChunks.forEach(function (chunk) {
    source.push(chunk);
  });
  source.push(null); //Note: forEach synchronous call
};

describe('m-json-stream', function () {
  it('should parse single object single chunk', function (done) {
    let expectedObjects = [
      {
        a1: 4
      }
    ];
    let chunks = [
      '{"a1": 4}'
    ];

    assertPipe(chunks, expectedObjects, {}, done);
  });

  it('should parse single object multiple chunks', function (done) {
    let expectedObjects = [
      {
        a2: 4
      }
    ];
    let chunks = [
      '{"a2',
      '":',
      ' 4}'
    ];

    assertPipe(chunks, expectedObjects, {}, done);
  });

  it('should parse multiple objects single chunk', function (done) {
    let expectedObjects = [
      {
        a3: 4
      },
      {
        b3: 8
      }
    ];
    let chunks = [
      '{"a3": 4}\n{"b3": 8}'
    ];

    assertPipe(chunks, expectedObjects, {}, done);
  });

  it('should parse multiple objects multiple chunks', function (done) {
    let expectedObjects = [
      {
        a4: 4
      },
      {
        b4: 8
      }
    ];
    let chunks = [
      '{"a4"',
      ': 4}\n',
      '{"b4',
      '":',
      ' 8}'
    ];

    assertPipe(chunks, expectedObjects, {}, done);
  });

  it('should parse no objects single chunk with error in first line (strict)', function (done) {
    let expectedObjects = [];
    let chunks = [
      '{"a5": 4,}\n{"b5": 8}'
    ];

    assertPipe(chunks, expectedObjects, {}, done);
  });

  it('should parse first object single chunk with error in second line (strict)', function (done) {
    let expectedObjects = [
      {
        a6: 4
      }
    ];
    let chunks = [
      '{"a6": 4}\n{"b6": 8,}'
    ];

    assertPipe(chunks, expectedObjects, {}, done);
  });

  it('should parse second object single chunk with error in first line (non-strict)', function (done) {
    let expectedObjects = [
      {
        b7: 8
      }
    ];
    let chunks = [
      '{"a7": 4,}\n{"b7": 8}'
    ];

    assertPipe(chunks, expectedObjects, {strict: false}, done);
  });

  it('should parse multiple objects multiple chunks with \\r', function (done) {
    let expectedObjects = [
      {
        a8: 4
      },
      {
        b8: 8
      }
    ];
    let chunks = [
      '{"a8"',
      ': 4}\r\n',
      '{"b8',
      '":',
      ' 8}'
    ];

    assertPipe(chunks, expectedObjects, {}, done);
  });

  it('should parse multiple objectst multiple chunks, first chunk bigger than initial buffer', function (done) {
    let expectedObjects = [
      {
        a9: 4
      },
      {
        b9: 8
      }
    ];
    let chunks = [
      '{"a9": ',
      '4}\n{"b9": 8',
      '}'
    ];

    assertPipe(chunks, expectedObjects, {initialBufferSize: 4}, done);
  });
});
