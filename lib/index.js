import util from 'util';
import { Transform } from 'stream';

function JsonStream({
  strict: strict = true,
  encoding: encoding = 'utf8',
  initialBufferSize: initialBufferSize = 64
} = {}) {
  if (!(this instanceof JsonStream)) {
    return new JsonStream();
  }

  Transform.call(this, {
    readableObjectMode: true
  });

  this._strict = strict;
  this._encoding = encoding;
  this._buffer = new Buffer(initialBufferSize);
  this._bufferDataLength = 0;
}

util.inherits(JsonStream, Transform);

let pushObject = function (line) {
  let object;
  try {
    object = JSON.parse(line);
  } catch (error) {
    if (this._strict) {
      this.emit('error', error);
      return false;
    } else {
      return true;
    }
  }

  this.push(object);

  return true;
};

JsonStream.prototype._transform = function (chunk, encoding, done) {
  if (chunk.length + this._bufferDataLength < this._buffer.length) {
    chunk.copy(this._buffer, this._bufferDataLength);
    this._bufferDataLength += chunk.length;
  } else { //existing buffer is not sufficient, create new one
    this._buffer = Buffer.concat([
      this._buffer.slice(0, this._bufferDataLength),
      chunk
    ]);
    this._bufferDataLength = this._buffer.length;
  }

  let i = 0;
  let start = 0;
  while (i < this._bufferDataLength) {
    //ignore \r
    if (this._buffer[i] === 13) {
      i++;
    }

    if (this._buffer[i] === 10) {
      let line = this._buffer.slice(start, i).toString(this._encoding);

      start = i + 1;

      if (!pushObject.call(this, line)) {
        return;
      }
    }

    i++;
  }


  if (start !== 0) {
    this._buffer = this._buffer.slice(start);
    this._bufferDataLength -= start;
  }

  done();
};

JsonStream.prototype._flush = function (done) {
  let remainder = this._buffer.toString(this._encoding, 0, this._bufferDataLength).trim();
  if (remainder.length > 0) {
    if (!pushObject.call(this, remainder)) {
      return;
    }
  }

  done();
};

export default JsonStream;
