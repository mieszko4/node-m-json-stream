'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _util = require('util');

var _util2 = _interopRequireDefault(_util);

var _stream = require('stream');

function JsonStream() {
  var _ref = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

  var _ref$strict = _ref.strict;
  var strict = _ref$strict === undefined ? true : _ref$strict;
  var _ref$encoding = _ref.encoding;
  var encoding = _ref$encoding === undefined ? 'utf8' : _ref$encoding;
  var _ref$initialBufferSize = _ref.initialBufferSize;
  var initialBufferSize = _ref$initialBufferSize === undefined ? 64 : _ref$initialBufferSize;

  if (!(this instanceof JsonStream)) {
    return new JsonStream();
  }

  _stream.Transform.call(this, {
    readableObjectMode: true
  });

  this._strict = strict;
  this._encoding = encoding;
  this._buffer = new Buffer(initialBufferSize);
  this._bufferDataLength = 0;
}

_util2['default'].inherits(JsonStream, _stream.Transform);

var pushObject = function pushObject(line) {
  var object = undefined;
  try {
    object = JSON.parse(line);
  } catch (error) {
    if (this._strict) {
      this.emit('error', error);
      return false;
    } else {
      return true;
    }
  }

  this.push(object);

  return true;
};

JsonStream.prototype._transform = function (chunk, encoding, done) {
  if (chunk.length + this._bufferDataLength < this._buffer.length) {
    chunk.copy(this._buffer, this._bufferDataLength);
    this._bufferDataLength += chunk.length;
  } else {
    //existing buffer is not sufficient, create new one
    this._buffer = Buffer.concat([this._buffer.slice(0, this._bufferDataLength), chunk]);
    this._bufferDataLength = this._buffer.length;
  }

  var i = 0;
  var start = 0;
  while (i < this._bufferDataLength) {
    //ignore \r
    if (this._buffer[i] === 13) {
      i++;
    }

    if (this._buffer[i] === 10) {
      var line = this._buffer.slice(start, i).toString(this._encoding);

      start = i + 1;

      if (!pushObject.call(this, line)) {
        return;
      }
    }

    i++;
  }

  if (start !== 0) {
    this._buffer = this._buffer.slice(start);
    this._bufferDataLength -= start;
  }

  done();
};

JsonStream.prototype._flush = function (done) {
  var remainder = this._buffer.toString(this._encoding, 0, this._bufferDataLength).trim();
  if (remainder.length > 0) {
    if (!pushObject.call(this, remainder)) {
      return;
    }
  }

  done();
};

exports['default'] = JsonStream;
module.exports = exports['default'];